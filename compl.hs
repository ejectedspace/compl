{-#LANGUAGE ScopedTypeVariables#-}
{-# LANGUAGE LambdaCase #-}

import Prelude hiding ((<>))

import System.Environment
import System.Directory
import System.File.Tree
import System.Process
import System.Exit
import System.IO

import Data.Foldable
import Data.Maybe
import Data.List

import Options.Applicative hiding ((<>))

(<>) :: Monoid m => m -> m -> m
(<>) = mappend

data Action = Start String
            | Stop String
            | Restart String
            | Pull String
            | Update String
            | New String String

parserInfo' = info' parser' "compl - a docker compose wrapper"
  where
    parser' :: Parser Action
    parser' = (subparser . foldMap command')
        [ ("start", "Start a Stack", Start <$> pStackPath)
        , ("stop", "Stop a Stack", Stop <$> pStackPath)
        , ("restart", "Restart a Stack", Restart <$> pStackPath)
        , ("pull", "Pull images for a Stack", Pull <$> pStackPath)
        , ("update", "Pull new images and Restart Stack", Update <$> pStackPath)
        , ("new", "Bootstrap a new stack", New <$> pStackPath <*> pTemplatePath )
        ]
    
    pStackPath :: Parser String
    pStackPath =
        strArgument $ metavar "STACK" 
                      <> help "Stack to use for Action"

    pTemplatePath :: Parser String
    pTemplatePath =
        strOption $ long "template"
                    <> short 't'
                    <> value "default"
                    <> metavar "TEMPLATE"
                    <> help "Path to a template compose stack to use for bootstrapping"
    
    command' (cmdName,desc,parser) = 
        command cmdName (info' parser desc)
    
    info' :: Parser a -> String -> ParserInfo a
    info' p desc = info 
        (helper <*> p) 
        (fullDesc <> progDesc desc)

run :: Action -> IO()
run = \case
    Start stack     -> execCompose stack ["up", "-d"]
    Stop stack      -> execCompose stack ["down"]
    Restart stack   -> execCompose stack ["down"]
                      >> execCompose stack ["up", "-d"]
    Pull stack      -> execCompose stack ["pull"]
    Update stack    -> execCompose stack ["pull"]
                      >> execCompose stack ["up", "-d"]
    New stack template      -> bootstrapCompose stack template

main :: IO ()
main = execParser parserInfo' >>= run

execCompose :: String -> [String] -> IO ()
execCompose path action = do
    stackBasePathEnv <- lookupEnv "STACK_BASE_PATH"
    let stackBasePath = fromMaybe "/srv/" stackBasePathEnv
    r <- doesFileExist $ stackBasePath ++ path ++ "/docker-compose.yml"
    if r
    then do
        (_, Just hout, _, _) <- createProcess (proc "docker-compose" action)
                                          {std_out = CreatePipe}
                                          {cwd = Just $ stackBasePath ++ path}
        dateOut <- hGetContents hout
        putStrLn dateOut
    else error "Stack is missing a docker-compose.yml file"

bootstrapCompose :: String -> String -> IO ()
bootstrapCompose path template = do
    stackBasePathEnv <- lookupEnv "STACK_BASE_PATH"
    let stackBasePath = fromMaybe "/srv/" stackBasePathEnv
    doesDirectoryExist (stackBasePath ++ path) >>= \case
        True -> do
            putStrLn "Stack already exists. Override? (y/N):"
            yn <- getChar
            if yn `elem` ['y', 'Y']
            then copyTemplate (stackBasePath ++ path) template
            else exitFailure
        False -> copyTemplate (stackBasePath ++ path) template

copyTemplate :: String -> String -> IO()
copyTemplate stack template
    | '/' `elem` template = copyTemplate' stack template
    | otherwise = copyTemplate' stack ("/usr/share/compl/" ++ template)

copyTemplate' :: String -> String -> IO()
copyTemplate' stack template = do
    dir <- doesDirectoryExist (template)
    if dir
    then do
        templateDir <- getDirectory template
        copyTo stack templateDir
        putStrLn $ "Copied Template to " ++ stack
    else error "Template could not be found"

